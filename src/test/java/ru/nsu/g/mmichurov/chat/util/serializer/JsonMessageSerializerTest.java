package ru.nsu.g.mmichurov.chat.util.serializer;

import org.junit.jupiter.api.Test;
import ru.nsu.g.mmichurov.chat.util.message.ClientMessage;
import ru.nsu.g.mmichurov.chat.util.message.ServerMessage;
import ru.nsu.g.mmichurov.chat.util.serializer.exception.DeserializationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class JsonMessageSerializerTest {
    static JsonMessageSerializer j = new JsonMessageSerializer();

    static class ServerMessageTest {

        static class EventTest {

            static class MessageTest {

                @Test
                void serialize() {
                    var from = "John Doe";
                    var message = "Hello world";
                    var obj = new ServerMessage.Event.Message(from, message);
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var expected = String.format(
                            "{\"type\":\"%s\",\"body\":{\"type\":\"%s\",\"body\":{\"from\":\"%s\",\"message\":\"%s\"}}}",
                            ServerMessage.Event.class.getSimpleName(),
                            ServerMessage.Event.Message.class.getSimpleName(),
                            from,
                            message
                    );

                    assertEquals(expected, new String(jsonBytes));
                }

                @Test
                void deserialize() throws DeserializationException {
                    var from = "John Doe";
                    var message = "Hello world";
                    var obj = new ServerMessage.Event.Message(from, message);
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var deserialized = j.deserialize(jsonBytes, ServerMessage.class);

                    assertTrue(deserialized instanceof ServerMessage.Event.Message);

                    obj = (ServerMessage.Event.Message) deserialized;

                    assertEquals(from, obj.from);
                    assertEquals(message, obj.message);
                }

            }

            static class UserLoginTest {

                @Test
                void serialize() {
                    var name = "John Doe";
                    var obj = new ServerMessage.Event.UserLogin(name);
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var expected = String.format(
                            "{\"type\":\"%s\",\"body\":{\"type\":\"%s\",\"body\":{\"name\":\"%s\"}}}",
                            ServerMessage.Event.class.getSimpleName(),
                            ServerMessage.Event.UserLogin.class.getSimpleName(),
                            name
                    );

                    assertEquals(expected, new String(jsonBytes));
                }

                @Test
                void deserialize() throws DeserializationException {
                    var name = "John Doe";
                    var obj = new ServerMessage.Event.UserLogin(name);
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var deserialized = j.deserialize(jsonBytes, ServerMessage.class);

                    assertTrue(deserialized instanceof ServerMessage.Event.UserLogin);

                    obj = (ServerMessage.Event.UserLogin) deserialized;

                    assertEquals(name, obj.name);
                }
            }

            static class UserLogoutTest {

                @Test
                void serialize() {
                    var name = "John Doe";
                    var obj = new ServerMessage.Event.UserLogout(name);
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var expected = String.format(
                            "{\"type\":\"%s\",\"body\":{\"type\":\"%s\",\"body\":{\"name\":\"%s\"}}}",
                            ServerMessage.Event.class.getSimpleName(),
                            ServerMessage.Event.UserLogout.class.getSimpleName(),
                            name
                    );

                    assertEquals(expected, new String(jsonBytes));
                }

                @Test
                void deserialize() throws DeserializationException {
                    var name = "John Doe";
                    var obj = new ServerMessage.Event.UserLogout(name);
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var deserialized = j.deserialize(jsonBytes, ServerMessage.class);

                    assertTrue(deserialized instanceof ServerMessage.Event.UserLogout);

                    obj = (ServerMessage.Event.UserLogout) deserialized;

                    assertEquals(name, obj.name);
                }
            }
        }

        static class AnswerTest {

            static class ErrorTest {

                @Test
                void serialize() {
                    var message = "John Doe";
                    var obj = new ServerMessage.Answer.Error(message);
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var expected = String.format(
                            "{\"type\":\"%s\",\"body\":{\"type\":\"%s\",\"body\":{\"message\":\"%s\"}}}",
                            ServerMessage.Answer.class.getSimpleName(),
                            ServerMessage.Answer.Error.class.getSimpleName(),
                            message
                    );

                    assertEquals(expected, new String(jsonBytes));
                }

                @Test
                void deserialize() throws DeserializationException {
                    var message = "John Doe";
                    var obj = new ServerMessage.Answer.Error(message);
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var deserialized = j.deserialize(jsonBytes, ServerMessage.class);

                    assertTrue(deserialized instanceof ServerMessage.Answer.Error);

                    obj = (ServerMessage.Answer.Error) deserialized;

                    assertEquals(message, obj.message);
                }

            }

            static class SuccessTest {

                static class SessionTest {

                    @Test
                    void serialize() {
                        var session = 122345;
                        var obj = new ServerMessage.Answer.Success.Session(session);
                        var jsonBytes = j.serialize(obj, ServerMessage.class);

                        var expected = String.format(
                                "{\"type\":\"%s\",\"body\":{\"type\":\"%s\",\"body\":{\"type\":\"%s\",\"body\":{\"session\":%d}}}}",
                                ServerMessage.Answer.class.getSimpleName(),
                                ServerMessage.Answer.Success.class.getSimpleName(),
                                ServerMessage.Answer.Success.Session.class.getSimpleName(),
                                session
                        );

                        assertEquals(expected, new String(jsonBytes));
                    }

                    @Test
                    void deserialize() throws DeserializationException {
                        var session = 122345;
                        var obj = new ServerMessage.Answer.Success.Session(session);
                        var jsonBytes = j.serialize(obj, ServerMessage.class);

                        var deserialized = j.deserialize(jsonBytes, ServerMessage.class);

                        assertTrue(deserialized instanceof ServerMessage.Answer.Success.Session);

                        obj = (ServerMessage.Answer.Success.Session) deserialized;

                        assertEquals(session, obj.session);
                    }
                }

                static class UsersListTest {

                    @Test
                    void serialize() {
                        var listusers = new ServerMessage.Answer.Success.UsersList.User[]{
                                new ServerMessage.Answer.Success.UsersList.User("John Doe", "Nice Client"),
                                new ServerMessage.Answer.Success.UsersList.User("Mick Gordon", "Guitar Hero Controller")
                        };
                        var obj = new ServerMessage.Answer.Success.UsersList(listusers);
                        var jsonBytes = j.serialize(obj, ServerMessage.class);

                        var expected = String.format(
                                "{\"type\":\"%s\",\"body\":{\"type\":\"%s\",\"body\":{\"type\":\"%s\",\"body\":" +
                                        "{\"listusers\":[{\"name\":\"%s\",\"type\":\"%s\"},{\"name\":\"%s\",\"type\":\"%s\"}]}}}}",
                                ServerMessage.Answer.class.getSimpleName(),
                                ServerMessage.Answer.Success.class.getSimpleName(),
                                ServerMessage.Answer.Success.UsersList.class.getSimpleName(),
                                listusers[0].name, listusers[0].type,
                                listusers[1].name, listusers[1].type
                        );

                        assertEquals(expected, new String(jsonBytes));
                    }

                    @Test
                    void deserialize() throws DeserializationException {
                        var listusers = new ServerMessage.Answer.Success.UsersList.User[]{
                                new ServerMessage.Answer.Success.UsersList.User("John Doe", "Nice Client"),
                                new ServerMessage.Answer.Success.UsersList.User("Mick Gordon", "Guitar Hero Controller")
                        };
                        var obj = new ServerMessage.Answer.Success.UsersList(listusers);
                        var jsonBytes = j.serialize(obj, ServerMessage.class);

                        var deserialized = j.deserialize(jsonBytes, ServerMessage.class);

                        assertTrue(deserialized instanceof ServerMessage.Answer.Success.UsersList);

                        obj = (ServerMessage.Answer.Success.UsersList) deserialized;

                        assertEquals(listusers[0].name, obj.listusers[0].name);
                        assertEquals(listusers[0].type, obj.listusers[0].type);
                        assertEquals(listusers[1].name, obj.listusers[1].name);
                        assertEquals(listusers[1].type, obj.listusers[1].type);
                    }
                }

                @Test
                void serialize() {
                    var obj = new ServerMessage.Answer.Success();
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var expected = String.format(
                            "{\"type\":\"%s\",\"body\":{\"type\":\"%s\",\"body\":{}}}",
                            ServerMessage.Answer.class.getSimpleName(),
                            ServerMessage.Answer.Success.class.getSimpleName()
                    );

                    assertEquals(expected, new String(jsonBytes));
                }

                @Test
                void deserialize() throws DeserializationException {
                    var obj = new ServerMessage.Answer.Success();
                    var jsonBytes = j.serialize(obj, ServerMessage.class);

                    var deserialized = j.deserialize(jsonBytes, ServerMessage.class);

                    assertTrue(deserialized instanceof ServerMessage.Answer.Success);
                }
            }
        }
    }

    static class ClientMessageTest {

        static class LoginTest {

            @Test
            void serialize() {
                var name = "John Doe";
                var type = "Nice Client";
                var obj = new ClientMessage.Login(name, type);
                var jsonBytes = j.serialize(obj, ClientMessage.class);

                var expected = String.format(
                        "{\"type\":\"%s\",\"body\":{\"name\":\"%s\",\"type\":\"%s\"}}",
                        ClientMessage.Login.class.getSimpleName(),
                        name,
                        type
                );

                assertEquals(expected, new String(jsonBytes));
            }

            @Test
            void deserialize() throws DeserializationException {
                var name = "John Doe";
                var type = "Nice Client";
                var obj = new ClientMessage.Login(name, type);
                var jsonBytes = j.serialize(obj, ClientMessage.class);

                var deserialized = j.deserialize(jsonBytes, ClientMessage.class);

                assertTrue(deserialized instanceof ClientMessage.Login);

                obj = (ClientMessage.Login) deserialized;

                assertEquals(name, obj.name);
                assertEquals(type, obj.type);
            }
        }

        static class LogoutTest {

            @Test
            void serialize() {
                var session = 12345;
                var obj = new ClientMessage.Logout(session);
                var jsonBytes = j.serialize(obj, ClientMessage.class);

                var expected = String.format(
                        "{\"type\":\"%s\",\"body\":{\"session\":%d}}",
                        ClientMessage.Logout.class.getSimpleName(),
                        session
                );

                assertEquals(expected, new String(jsonBytes));
            }

            @Test
            void deserialize() throws DeserializationException {
                var session = 12345;
                var obj = new ClientMessage.Logout(session);
                var jsonBytes = j.serialize(obj, ClientMessage.class);

                var deserialized = j.deserialize(jsonBytes, ClientMessage.class);

                assertTrue(deserialized instanceof ClientMessage.Logout);

                obj = (ClientMessage.Logout) deserialized;

                assertEquals(session, obj.session);
            }
        }

        static class ListTest {

            @Test
            void serialize() {
                var session = 12345;
                var obj = new ClientMessage.List(session);
                var jsonBytes = j.serialize(obj, ClientMessage.class);

                var expected = String.format(
                        "{\"type\":\"%s\",\"body\":{\"session\":%d}}",
                        ClientMessage.List.class.getSimpleName(),
                        session
                );

                assertEquals(expected, new String(jsonBytes));
            }

            @Test
            void deserialize() throws DeserializationException {
                var session = 12345;
                var obj = new ClientMessage.List(session);
                var jsonBytes = j.serialize(obj, ClientMessage.class);

                var deserialized = j.deserialize(jsonBytes, ClientMessage.class);

                assertTrue(deserialized instanceof ClientMessage.List);

                obj = (ClientMessage.List) deserialized;

                assertEquals(session, obj.session);
            }
        }

        static class MessageTest {

            @Test
            void serialize() {
                var message = "Hello World";
                var session = 12345;
                var obj = new ClientMessage.Message(message, session);
                var jsonBytes = j.serialize(obj, ClientMessage.class);

                var expected = String.format(
                        "{\"type\":\"%s\",\"body\":{\"message\":\"%s\",\"session\":%d}}",
                        ClientMessage.Message.class.getSimpleName(),
                        message,
                        session
                );

                assertEquals(expected, new String(jsonBytes));
            }

            @Test
            void deserialize() throws DeserializationException {
                var message = "Hello World";
                var session = 12345;
                var obj = new ClientMessage.Message(message, session);
                var jsonBytes = j.serialize(obj, ClientMessage.class);

                var deserialized = j.deserialize(jsonBytes, ClientMessage.class);

                assertTrue(deserialized instanceof ClientMessage.Message);

                obj = (ClientMessage.Message) deserialized;

                assertEquals(message, obj.message);
                assertEquals(session, obj.session);
            }
        }
    }
}