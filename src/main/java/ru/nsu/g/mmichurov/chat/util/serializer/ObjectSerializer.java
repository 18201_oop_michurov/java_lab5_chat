package ru.nsu.g.mmichurov.chat.util.serializer;

import ru.nsu.g.mmichurov.chat.util.serializer.exception.DeserializationException;

import java.io.*;

public class ObjectSerializer implements Serializer {
    @Override
    public byte[] serialize(Object object, Class<?> objectClass) throws IOException {
        var byteStream = new ByteArrayOutputStream();
        var os = new ObjectOutputStream(byteStream);

        os.writeObject(object);
        os.flush();

        return byteStream.toByteArray();
    }

    @Override
    public Object deserialize(byte[] bytes, Class<?> objectClass) throws IOException {
        var byteStream = new ByteArrayInputStream(bytes);

        try {
            var is = new ObjectInputStream(byteStream);
            var obj = is.readObject();

            if (!objectClass.isInstance(obj)) {
                throw new DeserializationException("Class missmatch: expected " + objectClass.getName()
                        + ", got " + obj.getClass().getName()
                );
            }

            return obj;
        } catch (ClassNotFoundException | IOException e) {
            throw new DeserializationException(e);
        }
    }
}
