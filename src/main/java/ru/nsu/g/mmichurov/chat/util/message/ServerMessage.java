package ru.nsu.g.mmichurov.chat.util.message;

import java.io.Serializable;

public interface ServerMessage extends Serializable {

   interface Event extends ServerMessage {

        final class UserLogin implements Event {
            public final String name;

            public UserLogin(String name) {
                this.name = name;
            }
        }

        final class UserLogout implements Event {
            public final String name;

            public UserLogout(String name) {
                this.name = name;
            }
        }

        final class Message implements Event {
            public final String from;
            public final String message;

            public Message(
                    String from,
                    String message) {
                this.from = from;
                this.message = message;
            }
        }
    }

    interface Answer extends ServerMessage {

        class Success implements Answer {

            public static final class Session extends Success {
                public final int session;

                public Session(int session) {
                    this.session = session;
                }
            }

            public static final class UsersList extends Success {
                public final User[] listusers;

                public UsersList(User[] listusers) {
                    this.listusers = listusers;
                }

                public static final class User implements Serializable {
                    public final String name;
                    public final String type;

                    public User(
                            String name,
                            String type) {
                        this.name = name;
                        this.type = type;
                    }
                }
            }
        }

        final class Error implements Answer {
            public final String message;

            public Error(String message) {
                this.message = message;
            }
        }
    }
}
