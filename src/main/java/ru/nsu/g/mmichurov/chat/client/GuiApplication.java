package ru.nsu.g.mmichurov.chat.client;

import ru.nsu.g.mmichurov.chat.client.ui.gui.ClientWindow;
import ru.nsu.g.mmichurov.chat.util.ArgumentParser;
import ru.nsu.g.mmichurov.chat.util.serializer.JsonMessageSerializer;

import java.util.NoSuchElementException;

public class GuiApplication {
    public static void main(String[] args) {
        var useJson = new ArgumentParser.Flag('j', "json");
        var help = new ArgumentParser.Flag('h', "help");
        var parser = new ArgumentParser(useJson, help);
        try {
            parser.parse(args);
        } catch (IllegalArgumentException | NoSuchElementException e) {
            System.err.println(e.getMessage());
        }

        if (help.isFound()) {
            printHelp();
            return;
        }

        if (useJson.isFound()) {
            Client.useSerializer(new JsonMessageSerializer());
        }

        new ClientWindow().run();
    }

    static void printHelp() {
        System.out.print(
                "Text chat client.\n\n" +
                        "USAGE:\n    <executable> [KEYS]\n\nKEYS:" +
                        "\n\n    -j, --json            Client will use JSON data serialization. " +
                        "Client uses Java Object Serialization by default." +
                        "\n\n    -h, --help            Display help.\n\n"
        );
    }
}
