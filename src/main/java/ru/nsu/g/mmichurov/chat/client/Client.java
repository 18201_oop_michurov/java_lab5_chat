package ru.nsu.g.mmichurov.chat.client;

import ru.nsu.g.mmichurov.chat.client.ui.ClientUI;
import ru.nsu.g.mmichurov.chat.util.Metadata;
import ru.nsu.g.mmichurov.chat.util.connection.Connection;
import ru.nsu.g.mmichurov.chat.util.connection.ConnectionListener;
import ru.nsu.g.mmichurov.chat.util.connection.exception.ProtocolViolationException;
import ru.nsu.g.mmichurov.chat.util.connection.exception.RequestFailedException;
import ru.nsu.g.mmichurov.chat.util.message.ClientMessage;
import ru.nsu.g.mmichurov.chat.util.message.ServerMessage;
import ru.nsu.g.mmichurov.chat.util.serializer.ObjectSerializer;
import ru.nsu.g.mmichurov.chat.util.serializer.Serializer;

import java.io.IOException;
import java.net.InetAddress;

public class Client {
    public static final String type = Metadata.clientType + " Ver. " + Metadata.version;
    private static Serializer serializer = new ObjectSerializer();

    private ClientUI view;

    private Connection connection;
    private int session;
    private ServerMessage.Answer answer;
    private Thread listenerThread;

    public static void useSerializer(Serializer newSerializer) {
        serializer = newSerializer;
    }

    public static Client connect(
            InetAddress address,
            int port) throws IOException {
        var client = new Client();
        client.connection = new Connection(address, port);
        client.listenerThread = new Thread(client.new ClientConnectionListener(client.connection));
        client.listenerThread.start();

        return client;
    }

    public void setView(ClientUI ui) {
        this.view = ui;
    }

    private synchronized void waitForAnswer() {
        try {
            this.wait();
        } catch (InterruptedException ignored) {}
    }

    public void login(String userName)
            throws IOException, RequestFailedException {
        this.connection.send(serializer.serialize(
                new ClientMessage.Login(userName, type),
                ClientMessage.class
        ));
        this.waitForAnswer();
        try {
            if (this.answer instanceof ServerMessage.Answer.Error) {
                var error = (ServerMessage.Answer.Error) this.answer;
                throw new RequestFailedException("Server rejected login request: " + error.message);
            }
            if (!(this.answer instanceof ServerMessage.Answer.Success.Session)) {
                throw new ProtocolViolationException("Expected session data");
            }

            var session = (ServerMessage.Answer.Success.Session) this.answer;
            this.session = session.session;
        } finally {
            this.answer = null;
        }
    }

    public void logout()
            throws IOException, RequestFailedException {
        this.connection.send(serializer.serialize(new ClientMessage.Logout(this.session), ClientMessage.class));

        try {
            this.waitForAnswer();

            if (this.answer instanceof ServerMessage.Answer.Error) {
                var error = (ServerMessage.Answer.Error) this.answer;
                throw new RequestFailedException("Server rejected logout request: " + error.message);
            }
        } finally {
            this.answer = null;
        }
    }

    public void send(String message)
            throws IOException, RequestFailedException {
        this.connection.send(serializer.serialize(
                new ClientMessage.Message(message, this.session),
                ClientMessage.class
        ));
        try {
            this.waitForAnswer();

            if (this.answer instanceof ServerMessage.Answer.Error) {
                var error = (ServerMessage.Answer.Error) this.answer;
                throw new RequestFailedException("Server rejected message request: " + error.message);
            }

            this.answer = null;
        } finally {
            this.answer = null;
        }
    }

    public void requestUsersList()
            throws IOException, RequestFailedException {
        this.connection.send(serializer.serialize(
                new ClientMessage.List(this.session),
                ClientMessage.class
        ));
        try {
            this.waitForAnswer();

            if (this.answer instanceof ServerMessage.Answer.Error) {
                var error = (ServerMessage.Answer.Error) this.answer;
                throw new RequestFailedException("Server rejected list request: " + error.message);
            }
            if (!(this.answer instanceof ServerMessage.Answer.Success.UsersList)) {
                throw new ProtocolViolationException("Expected UsersList");
            }
        } finally {
            this.answer = null;
        }
    }

    public void disconnect()
            throws IOException {
        try {
            this.shutdown();
        } finally {
            try {
                this.listenerThread.join();
            } catch (InterruptedException ignored) {
            }
        }
    }

    private void shutdown() throws IOException {
        this.connection.close();
    }

    private final class ClientConnectionListener extends ConnectionListener {

        protected ClientConnectionListener(Connection connection) {
            super(connection);
        }

        @Override
        public void onDataReceived(byte[] data) throws ProtocolViolationException {
            synchronized (Client.this) {
                try {
                    var serverMessage = serializer.deserialize(data, ServerMessage.class);

                    if (serverMessage instanceof ServerMessage.Event) {
                        Client.this.view.display((ServerMessage.Event) serverMessage);
                    } else if (serverMessage instanceof ServerMessage.Answer) {
                        Client.this.answer = (ServerMessage.Answer) serverMessage;
                        Client.this.notify();
                        Client.this.view.display((ServerMessage.Answer) serverMessage);
                    } else {
                        Client.this.view.error("Invalid server data format: expected Event");
                    }

                } catch (IOException e) {
                    throw new ProtocolViolationException("Invalid server data format", e);
                }
            }
        }

        @Override
        public void onConnectionClosed() {
            Client.this.view.display("Connection closed.");
        }

        @Override
        public void onIOException(IOException e) {
            try {
                Client.this.shutdown();
            } catch (IOException ignored) {
            }
            synchronized (Client.this) {
                Client.this.notify();
            }
            Client.this.view.fatal(e.getMessage() + "\nShutting down");
        }

        @Override
        public void onProtocolViolationException(ProtocolViolationException e) {
            this.onIOException(e);
            /*Client.this.view.error("Protocol violation");
            synchronized (Client.this) {
                Client.this.notify();
            }*/
        }
    }
}
