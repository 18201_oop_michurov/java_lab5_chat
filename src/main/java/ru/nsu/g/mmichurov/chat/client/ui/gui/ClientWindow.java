package ru.nsu.g.mmichurov.chat.client.ui.gui;

import ru.nsu.g.mmichurov.chat.client.Client;
import ru.nsu.g.mmichurov.chat.client.ui.ClientUI;
import ru.nsu.g.mmichurov.chat.client.ui.util.Utility;
import ru.nsu.g.mmichurov.chat.util.connection.exception.RequestFailedException;
import ru.nsu.g.mmichurov.chat.util.message.ServerMessage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayDeque;
import java.util.Queue;

public class ClientWindow extends JFrame implements ClientUI, Runnable {
    private Client client = null;

    private static final String CONNECT_PANEL = "Connect";
    private static final String LOGIN_PANEL = "Login";
    private static final String CHAT_PANEL = "Chat";

    private final ConnectPanel connectPanel = new ConnectPanel();
    private final LoginPanel loginPanel = new LoginPanel();
    private final ChatPanel chatPanel = new ChatPanel();

    private JPanel mainPanel = new JPanel(new CardLayout());

    public ClientWindow() {
        super(Client.type);

        this.addComponentsToPane(this.getContentPane());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (ClientWindow.this.client != null) {
                    try {
                        ClientWindow.this.client.logout();
                    } catch (IOException | RequestFailedException ignored) {
                    }
                }
            }
        });
        this.setResizable(false);
        this.pack();
        this.setLocationRelativeTo(null);
    }

    private final class ConnectPanel extends JPanel {
        final JTextField addressField = new JTextField("127.0.0.1");
        final JTextField portField = new JTextField("11111");

        ConnectPanel() {
            this.init();
        }

        void tryConnect(ActionEvent e) {
            if (!Utility.isValidIpAddress(this.addressField.getText())) {
                ClientWindow.this.error("IP address is invalid");
            }
            if (!Utility.isValidPort(this.portField.getText())) {
                ClientWindow.this.error("Port value is invalid");
            }

            try {
                ClientWindow.this.client = Client.connect(
                        InetAddress.getByName(this.addressField.getText()),
                        Integer.parseInt(this.portField.getText())
                );
                ClientWindow.this.client.setView(ClientWindow.this);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(ClientWindow.this, ex.getMessage());

                return;
            }

            var cards = (CardLayout) ClientWindow.this.mainPanel.getLayout();
            cards.show(ClientWindow.this.mainPanel, LOGIN_PANEL);
        }

        void init() {
            this.setLayout(new BorderLayout());

            var connectionDataPanel = new JPanel();
            connectionDataPanel.setLayout(new BoxLayout(connectionDataPanel, BoxLayout.X_AXIS));

            this.addressField.addActionListener(this::tryConnect);
            this.portField.addActionListener(this::tryConnect);

            connectionDataPanel.add(this.addressField);
            connectionDataPanel.add(new JLabel(":"));
            connectionDataPanel.add(this.portField);

            this.add(connectionDataPanel, BorderLayout.CENTER);

            var connectButton = new JButton("Connect");

            connectButton.addActionListener(this::tryConnect);

            this.add(connectButton, BorderLayout.SOUTH);
        }
    }

    private final class LoginPanel extends JPanel {
        final JTextField userNameField = new JTextField();

        LoginPanel() {
            this.init();
        }

        void tryLogin(ActionEvent e) {
            var userName = this.userNameField.getText();

            if (userName.isBlank()) {
                ClientWindow.this.error("Invalid user name");
                return;
            }

            try {
                ClientWindow.this.client.login(userName);
            } catch (RequestFailedException | IOException ex) {
                return;
            }

            var cards = (CardLayout) ClientWindow.this.mainPanel.getLayout();
            cards.show(ClientWindow.this.mainPanel, CHAT_PANEL);
            ClientWindow.this.setSize(400, 300);
            ClientWindow.this.setMinimumSize(ClientWindow.this.getSize());
            ClientWindow.this.setResizable(true);
            ClientWindow.this.setLocationRelativeTo(null);

            ClientWindow.this.chatPanel.addServiceMessage(Utility.InputHandler.CommandHandler.handleLocal("/help"));

            try {
                ClientWindow.this.client.requestUsersList();
            } catch (RequestFailedException | IOException ignored) {
            }
        }

        void init() {
            this.setLayout(new BorderLayout());

            var loginDataPanel = new JPanel();

            loginDataPanel.setLayout(new BoxLayout(loginDataPanel, BoxLayout.X_AXIS));
            loginDataPanel.add(new JLabel("User name:"));
            loginDataPanel.add(this.userNameField);

            this.userNameField.addActionListener(this::tryLogin);

            this.add(loginDataPanel, BorderLayout.CENTER);

            var loginButton = new JButton("Login");
            loginButton.addActionListener(this::tryLogin);

            this.add(loginButton, BorderLayout.SOUTH);
        }
    }

    private final class ChatPanel extends JPanel {
        final JPanel mainList = new JPanel();
        final JScrollPane scroll = new JScrollPane(
                this.mainList,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        );
        final Queue<JPanel> messagesHistory = new ArrayDeque<>();

        final int MAXIMUM_HISTORY_SIZE = 500;


        ChatPanel() {
            this.init();
        }

        synchronized void addServiceMessage(String message) {
            JTextArea textArea = new JTextArea(message, 1, 1);

            textArea.setWrapStyleWord(true);
            textArea.setLineWrap(true);
            textArea.setOpaque(false);
            textArea.setEditable(false);
            textArea.setFocusable(true);
            textArea.setBackground(UIManager.getColor("Label.background"));
            textArea.setBorder(UIManager.getBorder("Label.border"));

            var panel = new JPanel(new BorderLayout());
            panel.add(textArea, BorderLayout.CENTER);

            this.mainList.add(panel);
            this.scroll.revalidate();

            try {
                Thread.sleep(20);
            } catch (InterruptedException ignored) {
            }

            SwingUtilities.invokeLater(() -> {
                var scrollBar = this.scroll.getVerticalScrollBar();
                scrollBar.setValue(scrollBar.getMaximum());
            });
        }

        synchronized void addMessage(String from, String message) {
            var panel = new JPanel(new BorderLayout());
            panel.add(new JLabel(from), BorderLayout.NORTH);

            var textArea = new JTextArea(message);

            textArea.setWrapStyleWord(true);
            textArea.setLineWrap(true);
            textArea.setOpaque(false);
            textArea.setEditable(false);
            textArea.setFocusable(true);
            textArea.setBackground(UIManager.getColor("Label.background"));
            textArea.setBorder(UIManager.getBorder("Label.border"));

            panel.add(textArea, BorderLayout.CENTER);

            this.mainList.add(panel);

            this.messagesHistory.add(panel);
            if (this.messagesHistory.size() > MAXIMUM_HISTORY_SIZE) {
                var oldest = this.messagesHistory.poll();
                this.mainList.remove(oldest);
            }

            this.scroll.revalidate();

            try {
                Thread.sleep(20);
            } catch (InterruptedException ignored) {
            }

            SwingUtilities.invokeLater(() -> {
                var scrollBar = this.scroll.getVerticalScrollBar();
                scrollBar.setValue(scrollBar.getMaximum());
            });
        }

        private final class MessageInputPanel extends JPanel {
            final JTextField messageField = new JTextField();

            MessageInputPanel() {
                this.init();
            }

            void onUserInput(ActionEvent actionEvent) {
                var input = this.messageField.getText().strip();
                this.messageField.setText("");

                if (input.isBlank()) {
                    return;
                }

                try {
                    ChatPanel.this.addMessage("You", input);
                    if (Utility.InputHandler.isCommand(input)) {
                        if (!Utility.InputHandler.CommandHandler.isValidCommand(input)) {
                            ClientWindow.this.display("Unknown command");
                        }
                        if (Utility.InputHandler.CommandHandler.isLocal(input)) {
                            ClientWindow.this.display(Utility.InputHandler.CommandHandler.handleLocal(input));
                        } else {
                            if ("/list".equals(input)) {
                                ClientWindow.this.client.requestUsersList();
                            } else if ("/logout".equals(input)) {
                                ClientWindow.this.client.logout();
                                ClientWindow.this.dispose();
                            }
                        }
                    } else {
                        ClientWindow.this.client.send(input);
                    }
                } catch (RequestFailedException e) {
                    ClientWindow.this.error(e.getMessage());
                } catch (IOException e) {
                    ClientWindow.this.fatal(e.getMessage());
                }
            }

            void init() {
                this.setLayout(new BorderLayout());

                var sendButton = new JButton("Send");

                this.messageField.addActionListener(this::onUserInput);
                sendButton.addActionListener(this::onUserInput);

                this.add(this.messageField, BorderLayout.CENTER);
                this.add(sendButton, BorderLayout.EAST);
            }
        }

        void init() {
            this.setLayout(new BorderLayout());

            this.mainList.setLayout(new BoxLayout(this.mainList, BoxLayout.Y_AXIS));

            this.add(this.scroll, BorderLayout.CENTER);

            var messageInputPanel = new MessageInputPanel();
            this.add(messageInputPanel, BorderLayout.SOUTH);
        }
    }

    private void addComponentsToPane(final Container pane) {
        this.mainPanel.add(this.connectPanel, CONNECT_PANEL);
        this.mainPanel.add(this.loginPanel, LOGIN_PANEL);
        this.mainPanel.add(this.chatPanel, CHAT_PANEL);

        pane.add(this.mainPanel);
    }

    @Override
    public void display(ServerMessage.Event event) {
        if (event instanceof ServerMessage.Event.Message) {
            var message = (ServerMessage.Event.Message) event;
            //this.display(String.format("%s: %s", message.from, message.message));
            this.display(message);
        } else if (event instanceof ServerMessage.Event.UserLogin) {
            var login = (ServerMessage.Event.UserLogin) event;
            this.display(String.format("%s connected", login.name));
        } else if (event instanceof ServerMessage.Event.UserLogout) {
            var logout = (ServerMessage.Event.UserLogout) event;
            this.display(String.format("%s disconnected", logout.name));
        }
    }

    @Override
    public void display(ServerMessage.Answer answer) {
        if (answer instanceof ServerMessage.Answer.Error) {
            var error = (ServerMessage.Answer.Error) answer;
            this.chatPanel.addServiceMessage(error.message);
            this.error(error.message);
        } else if (answer instanceof ServerMessage.Answer.Success) {
            if (answer instanceof ServerMessage.Answer.Success.UsersList) {
                var usersList = (ServerMessage.Answer.Success.UsersList) answer;

                var sb = new StringBuilder();

                for (int i = 0; i < usersList.listusers.length; ++i) {
                    var user = usersList.listusers[i];
                    sb.append(String.format("%s (%s)", user.name, user.type));

                    if (i != usersList.listusers.length - 1) {
                        sb.append('\n');
                    }
                }

                this.chatPanel.addMessage("Users online:", sb.toString());
            }
        }
    }

    @Override
    public void display(String message) {
        this.chatPanel.addServiceMessage(message);
    }

    public void display(ServerMessage.Event.Message message) {
        this.chatPanel.addMessage(message.from, message.message);
    }

    @Override
    public void error(String message) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(this, message));
    }

    @Override
    public void fatal(String message) {
        SwingUtilities.invokeLater(() -> {
            JOptionPane.showMessageDialog(this, message);
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });
    }

    @Override
    public void run() {
        SwingUtilities.invokeLater(() -> this.setVisible(true));
    }
}
