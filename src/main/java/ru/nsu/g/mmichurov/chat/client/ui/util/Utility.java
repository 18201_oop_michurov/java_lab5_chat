package ru.nsu.g.mmichurov.chat.client.ui.util;

import java.util.regex.Pattern;

public class Utility {

    public static boolean isValidIpAddress(String address) {
        var pattern = Pattern.compile(
                "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}" +
                        "([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
        );

        return pattern.matcher(address).matches();
    }

    public static boolean isValidPort(String port) {
        try {
            var portValue = Integer.parseInt(port);

            return portValue >= 1 && portValue <= 65535;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static final class InputHandler {

        public static final class CommandHandler {

            public static boolean isValidCommand(String message) {
                return isCommand(message) &&
                        ("/logout".equals(message) || "/list".equals(message) || "/help".equals(message));
            }

            public static boolean isLocal(String message) {
                return "/help".equals(message);
            }

            public static String handleLocal(String message) {
                String result = null;
                if ("/help".equals(message)) {
                    result = "\t/help - list available commands\n" +
                            "\t/list - list connected users\n" +
                            "\t/logout - logout and exit";
                }

                return result;
            }

        }

        public static boolean isCommand(String message) {
            return message.charAt(0) == '/';
        }
    }
}
