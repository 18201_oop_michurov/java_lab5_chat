package ru.nsu.g.mmichurov.chat.util.serializer;

import com.google.gson.*;
import ru.nsu.g.mmichurov.chat.util.message.ClientMessage;
import ru.nsu.g.mmichurov.chat.util.message.ServerMessage;
import ru.nsu.g.mmichurov.chat.util.serializer.exception.DeserializationException;

import java.lang.reflect.Type;

public class JsonMessageSerializer implements Serializer {
    private static final Gson gson;

    static {
        var gb = new GsonBuilder();
        gb.registerTypeAdapter(ClientMessage.class, new ClientMessageSerializer());
        gb.registerTypeAdapter(ServerMessage.class, new ServerMessageSerializer());
        gb.registerTypeAdapter(ServerMessage.Event.class, new ServerMessageSerializer.ServerEventSerializer());
        gb.registerTypeAdapter(ServerMessage.Answer.class, new ServerMessageSerializer.ServerAnswerSerializer());
        gb.registerTypeAdapter(
                ServerMessage.Answer.Success.class,
                new ServerMessageSerializer.ServerAnswerSerializer.SuccessSerializer()
        );

        gb.registerTypeAdapter(ClientMessage.class, new ClientMessageDeserializer());
        gb.registerTypeAdapter(ServerMessage.class, new ServerMessageDeserializer());
        gb.registerTypeAdapter(ServerMessage.Event.class, new ServerMessageDeserializer.EventDeserializer());
        gb.registerTypeAdapter(ServerMessage.Answer.class, new ServerMessageDeserializer.AnswerDeserializer());
        gb.registerTypeAdapter(
                ServerMessage.Answer.Success.class,
                new ServerMessageDeserializer.AnswerDeserializer.SuccessDeserializer()
        );

        gson = gb.create();
    }

    private static final class ClientMessageSerializer implements JsonSerializer<ClientMessage> {

        @Override
        public JsonElement serialize(
                ClientMessage src,
                Type typeOfSrc,
                JsonSerializationContext context) {
            var obj = new JsonObject();

            obj.addProperty("type", src.getClass().getSimpleName());
            obj.add("body", context.serialize(src));

            return context.serialize(obj);
        }
    }

    private static final class ClientMessageDeserializer implements JsonDeserializer<ClientMessage> {
        private final static String baseName = "ru.nsu.g.mmichurov.chat.util.message.ClientMessage$";

        @Override
        public ClientMessage deserialize(
                JsonElement json,
                Type typeOfT,
                JsonDeserializationContext context) throws JsonParseException {
            var obj = json.getAsJsonObject();
            var type = obj.get("type").getAsString();

            try {
                return context.deserialize(obj.get("body"), Class.forName(baseName + type));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("Unknown message type: " + type, e);
            }
        }
    }

    private static final class ServerMessageSerializer implements JsonSerializer<ServerMessage> {

        @Override
        public JsonElement serialize(
                ServerMessage src,
                Type typeOfSrc,
                JsonSerializationContext context) {
            var obj = new JsonObject();
            Class<?> nextLevelInterface;
            String type;

            if (src instanceof ServerMessage.Answer) {
                nextLevelInterface = ServerMessage.Answer.class;
                type = ServerMessage.Answer.class.getSimpleName();
            } else if (src instanceof ServerMessage.Event) {
                nextLevelInterface = ServerMessage.Event.class;
                type = ServerMessage.Event.class.getSimpleName();
            } else {
                throw new RuntimeException("Cannot determine implemented interface");
            }

            obj.addProperty("type", type);
            obj.add("body", context.serialize(src, nextLevelInterface));

            return context.serialize(obj);
        }

        static final class ServerEventSerializer implements JsonSerializer<ServerMessage.Event> {

            @Override
            public JsonElement serialize(
                    ServerMessage.Event src,
                    Type typeOfSrc,
                    JsonSerializationContext context) {
                var obj = new JsonObject();

                obj.addProperty("type", src.getClass().getSimpleName());
                obj.add("body", context.serialize(src));

                return context.serialize(obj);
            }
        }

        static final class ServerAnswerSerializer implements JsonSerializer<ServerMessage.Answer> {

            @Override
            public JsonElement serialize(
                    ServerMessage.Answer src,
                    Type typeOfSrc,
                    JsonSerializationContext context) {
                var obj = new JsonObject();

                if (src instanceof ServerMessage.Answer.Success) {
                    obj.addProperty("type", ServerMessage.Answer.Success.class.getSimpleName());
                    obj.add("body", context.serialize(src, ServerMessage.Answer.Success.class));
                } else if (src instanceof ServerMessage.Answer.Error) {
                    obj.addProperty("type", ServerMessage.Answer.Error.class.getSimpleName());
                    obj.add("body", context.serialize(src));
                } else {
                    throw new RuntimeException("Cannot determine implemented interface");
                }

                return context.serialize(obj);
            }

            static final class SuccessSerializer implements JsonSerializer<ServerMessage.Answer.Success> {

                @Override
                public JsonElement serialize(
                        ServerMessage.Answer.Success src,
                        Type typeOfSrc,
                        JsonSerializationContext context) {
                    var obj = new JsonObject();

                    if (src instanceof ServerMessage.Answer.Success.Session
                            || src instanceof ServerMessage.Answer.Success.UsersList) {
                        obj.addProperty("type", src.getClass().getSimpleName());
                        obj.add("body", context.serialize(src));
                    }

                    return context.serialize(obj);
                }
            }
        }
    }

    private static final class ServerMessageDeserializer implements JsonDeserializer<ServerMessage> {
        private final static String baseName = "ru.nsu.g.mmichurov.chat.util.message.ServerMessage$";

        @Override
        public ServerMessage deserialize(
                JsonElement json,
                Type typeOfT,
                JsonDeserializationContext context) throws JsonParseException {
            var obj = json.getAsJsonObject();
            var type = obj.get("type").getAsString();

            try {
                return context.deserialize(obj.get("body"), Class.forName(baseName + type));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("Unknown message type: " + type, e);
            }
        }

        static final class AnswerDeserializer implements JsonDeserializer<ServerMessage.Answer> {
            private final static String baseName = "ru.nsu.g.mmichurov.chat.util.message.ServerMessage$Answer$";

            @Override
            public ServerMessage.Answer deserialize(
                    JsonElement json,
                    Type typeOfT,
                    JsonDeserializationContext context) throws JsonParseException {
                var obj = json.getAsJsonObject();
                var type = obj.get("type").getAsString();

                try {
                    return context.deserialize(obj.get("body"), Class.forName(baseName + type));
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException("Unknown answer type: " + type, e);
                }
            }

            static final class SuccessDeserializer implements JsonDeserializer<ServerMessage.Answer.Success> {
                private final static String baseName = "ru.nsu.g.mmichurov.chat.util." +
                        "message.ServerMessage$Answer$Success$";

                @Override
                public ServerMessage.Answer.Success deserialize(
                        JsonElement json,
                        Type typeOfT,
                        JsonDeserializationContext context) throws JsonParseException {
                    var obj = json.getAsJsonObject();
                    if (obj.entrySet().isEmpty()) {
                        return new ServerMessage.Answer.Success();
                    }
                    var type = obj.get("type").getAsString();

                    try {
                        return context.deserialize(obj.get("body"), Class.forName(baseName + type));
                    } catch (ClassNotFoundException e) {
                        throw new RuntimeException("Unknown success answer embedded data type: " + type, e);
                    }
                }
            }
        }

        static final class EventDeserializer implements JsonDeserializer<ServerMessage.Event> {
            private final static String baseName = "ru.nsu.g.mmichurov.chat.util.message.ServerMessage$Event$";

            @Override
            public ServerMessage.Event deserialize(
                    JsonElement json,
                    Type typeOfT,
                    JsonDeserializationContext context) throws JsonParseException {
                var obj = json.getAsJsonObject();
                var type = obj.get("type").getAsString();

                try {
                    return context.deserialize(obj.get("body"), Class.forName(baseName + type));
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException("Unknown event type: " + type, e);
                }
            }
        }
    }

    @Override
    public byte[] serialize(
            Object object,
            Class<?> objectClass) {
        return gson.toJson(object, objectClass).getBytes();
    }

    @Override
    public Object deserialize(
            byte[] bytes,
            Class<?> objectClass) throws DeserializationException {
        try {
            return gson.fromJson(new String(bytes), objectClass);
        } catch (Exception e) {
            throw new DeserializationException(e);
        }
    }
}
