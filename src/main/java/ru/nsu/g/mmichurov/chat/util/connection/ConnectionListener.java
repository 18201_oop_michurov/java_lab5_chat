package ru.nsu.g.mmichurov.chat.util.connection;

import ru.nsu.g.mmichurov.chat.util.connection.exception.ProtocolViolationException;

import java.io.EOFException;
import java.io.IOException;

public abstract class ConnectionListener implements Runnable {
    private final Connection connection;

    protected ConnectionListener(Connection connection) {
        this.connection = connection;
    }

    @Override
    public final void run() {
        while (true) {
            try {
                var input = this.connection.receive();

                this.onDataReceived(input);
            } catch (ProtocolViolationException e) {
                this.onProtocolViolationException(e);
                break;
            } catch (IOException e) {
                if (!this.connection.isClosed() && !(e instanceof EOFException)) {
                    this.onIOException(e);
                }
                try {
                    this.connection.close();
                } catch (IOException ignored) {}

                this.onConnectionClosed();
                break;
            }
        }
    }

    public abstract void onDataReceived(byte[] data) throws ProtocolViolationException;

    public abstract void onConnectionClosed();

    public abstract void onIOException(IOException e);

    public abstract void onProtocolViolationException(ProtocolViolationException e);
}
