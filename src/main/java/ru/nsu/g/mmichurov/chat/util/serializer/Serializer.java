package ru.nsu.g.mmichurov.chat.util.serializer;

import java.io.IOException;

public interface Serializer {
    byte[] serialize(Object object, Class<?> objectClass) throws IOException;

    Object deserialize(byte[] bytes, Class<?> objectClass) throws IOException;
}
