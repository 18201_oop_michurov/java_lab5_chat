package ru.nsu.g.mmichurov.chat.client.ui.cli;

import ru.nsu.g.mmichurov.chat.client.Client;
import ru.nsu.g.mmichurov.chat.client.ui.ClientUI;
import ru.nsu.g.mmichurov.chat.client.ui.util.Utility;
import ru.nsu.g.mmichurov.chat.util.connection.exception.RequestFailedException;
import ru.nsu.g.mmichurov.chat.util.message.ServerMessage;

import java.io.IOException;
import java.util.Scanner;

public class CommandLineInterface implements ClientUI, Runnable {
    private static final Scanner scanner = new Scanner(System.in);

    private Client client;

    private String name;

    public void bind(Client client) {
        this.client = client;
        this.client.setView(this);
    }

    public String readServerAddress() {
        while (true) {
            this.display("Enter server ip address: ");
            if (scanner.hasNextLine()) {
                var line = scanner.nextLine().strip();
                if (line.isBlank()) {
                    continue;
                }
                if (Utility.isValidIpAddress(line)) {
                    return line;
                } else {
                    this.display("Invalid address. Please try again.");
                }
            } else {
                return null;
            }
        }
    }

    public Integer readPort() {
        while (true) {
            this.display("Enter port: ");
            if (scanner.hasNextLine()) {
                var line = scanner.nextLine().strip();
                if (line.isBlank()) {
                    continue;
                }
                try {
                    return Integer.parseInt(line);
                } catch (NumberFormatException e) {
                    this.display("Invalid port value. Please try again.");
                }
            } else {
                return null;
            }
        }
    }

    public boolean readUserName() {
        while (true) {
            this.display("Login as: ");
            if (scanner.hasNextLine()) {
                var line = scanner.nextLine().strip();
                if (line.isBlank()) {
                    continue;
                }
                this.name = line;
                return true;
            } else {
                return false;
            }
        }
    }

    public String readInput() {
        while (true) {
            if (scanner.hasNextLine()) {
                var line = scanner.nextLine().strip();
                if (line.isBlank()) {
                    continue;
                }
                return line;
            } else {
                return null;
            }
        }
    }

    public boolean login() {
        while (true) {
            if (!this.readUserName()) {
                try {
                    this.client.disconnect();
                } catch (IOException ignored) {}
                return false;
            }
            try {
                this.client.login(this.name);
                break;
            } catch (IOException e) {
                this.error(e.getMessage());
                try {
                    this.client.disconnect();
                } catch (IOException ignored) {}
                return false;
            } catch (RequestFailedException ignored) {}
        }

        this.display(Utility.InputHandler.CommandHandler.handleLocal("/help"));
        try {
            this.client.requestUsersList();
        } catch (IOException | RequestFailedException ignored) {
        }

        return true;
    }

    public void logout() {
        try {
            this.client.logout();
        } catch (IOException | RequestFailedException ignored) {
        } finally {
            try {
                this.client.disconnect();
            } catch (IOException ignored) {}
        }
    }

    @Override
    public synchronized void display(ServerMessage.Event event) {
        if (event instanceof ServerMessage.Event.Message) {
            var message = (ServerMessage.Event.Message) event;
            this.display(String.format("%s: %s", message.from, message.message));
        } else if (event instanceof ServerMessage.Event.UserLogin) {
            var login = (ServerMessage.Event.UserLogin) event;
            this.display(String.format("%s connected", login.name));
        } else if (event instanceof ServerMessage.Event.UserLogout) {
            var logout = (ServerMessage.Event.UserLogout) event;
            this.display(String.format("%s disconnected", logout.name));
        }
    }

    @Override
    public synchronized void display(ServerMessage.Answer answer) {
        if (answer instanceof ServerMessage.Answer.Error) {
            var error = (ServerMessage.Answer.Error) answer;
            this.display(error.message);
        } else if (answer instanceof ServerMessage.Answer.Success) {
            if (answer instanceof ServerMessage.Answer.Success.UsersList) {
                var usersList = (ServerMessage.Answer.Success.UsersList) answer;

                this.display("Users online:");
                for (var user : usersList.listusers) {
                    this.display(String.format("%s (%s)", user.name, user.type));
                }
            }
        }
    }

    @Override
    public synchronized void display(String message) {
        if (message == null) {
            return;
        }

        System.out.println(message);
    }

    @Override
    public synchronized void error(String message) {
        if (message == null) {
            return;
        }

        System.err.println(message);
    }

    @Override
    public synchronized void fatal(String message) {
        this.error(message);
        System.exit(0);
    }

    @Override
    public void run() {
        outer:
        while (true) {
            var input = this.readInput();
            if (input == null) {
                break;
            }
            try {
                if (input.charAt(0) == '/') {
                    switch (input) {
                        case "/help":
                            this.display("Available commands:\n" +
                                    "\t/help - list available commands\n" +
                                    "\t/list - list connected users\n" +
                                    "\t/logout - logout and exit");
                            break;
                        case "/list":
                            this.client.requestUsersList();
                            break;
                        case "/logout":
                            break outer;
                        default:
                            this.error("Unknown command");
                    }
                } else {
                    this.client.send(input);
                }
            } catch (IOException e) {
                this.error(e.getMessage());
                return;
            } catch (RequestFailedException ignored) {
            }
        }
    }
}
