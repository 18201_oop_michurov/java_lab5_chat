package ru.nsu.g.mmichurov.chat.client;

import ru.nsu.g.mmichurov.chat.client.ui.cli.CommandLineInterface;
import ru.nsu.g.mmichurov.chat.util.ArgumentParser;
import ru.nsu.g.mmichurov.chat.util.serializer.JsonMessageSerializer;

import java.io.IOException;
import java.net.InetAddress;
import java.util.NoSuchElementException;

public class Application {

    public static void main(String[] args) {
        var useJson = new ArgumentParser.Flag('j', "json");
        var help = new ArgumentParser.Flag('h', "help");
        var parser = new ArgumentParser(useJson, help);
        try {
            parser.parse(args);
        } catch (IllegalArgumentException | NoSuchElementException e) {
            System.err.println(e.getMessage());
        }

        if (help.isFound()) {
            printHelp();
            return;
        }

        var con = new CommandLineInterface();

        String address;
        Integer port;

        if ((address = con.readServerAddress()) == null) {
            return;
        }
        if ((port = con.readPort()) == null) {
            return;
        }

        if (useJson.isFound()) {
            Client.useSerializer(new JsonMessageSerializer());
        }

        final Client client;
        try {
            client = Client.connect(InetAddress.getByName(address), port);
        } catch (IOException e) {
            System.err.printf("Failed to connect to %s:%d: %s", address, port, e.getMessage());
            return;
        }

        con.bind(client);
        if (!con.login()) {
            return;
        }
        con.run();
        con.logout();
    }

    static void printHelp() {
        System.out.print(
                "Text chat client.\n\n" +
                        "USAGE:\n    <executable> [KEYS]\n\nKEYS:" +
                        "\n\n    -j, --json            Client will use JSON data serialization. " +
                        "Client uses Java Object Serialization by default." +
                        "\n\n    -h, --help            Display help.\n\n"
        );
    }
}
