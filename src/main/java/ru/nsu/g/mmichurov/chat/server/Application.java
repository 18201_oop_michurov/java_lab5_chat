package ru.nsu.g.mmichurov.chat.server;

import ru.nsu.g.mmichurov.chat.util.ArgumentParser;
import ru.nsu.g.mmichurov.chat.util.serializer.JsonMessageSerializer;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.logging.LogManager;

public class Application {
    public static void main(String[] args) {
        try {
            var config = Application.class.getResourceAsStream("/logging.properties");
            if (config == null) {
                throw new NullPointerException("Cannot load " + "\"/logging.properties\"");
            }
            LogManager.getLogManager().readConfiguration(config);
        } catch (NullPointerException | IOException e) {
            System.out.println("Could not setup logger configuration: " + e.toString());
        }

        var portArgument = new ArgumentParser.IntegerArgument('p', "port", 11111);
        var useJson = new ArgumentParser.Flag('j', "json");
        var help = new ArgumentParser.Flag('h', "help");
        var parser = new ArgumentParser(portArgument, useJson, help);
        try {
            parser.parse(args);
        } catch (IllegalArgumentException | NoSuchElementException e) {
            System.err.println(e.getMessage());
            return;
        }

        if (help.isFound()) {
            printHelp();
            return;
        }

        if (useJson.isFound()) {
            Server.useSerializer(new JsonMessageSerializer());
        }

        try {
            var server = new Server(portArgument.getArgument());
            server.run();
        } catch (IOException | IllegalArgumentException e) {
            System.err.printf("Failed to run on port %d: %s\n", portArgument.getArgument(), e.getMessage());
        }
    }

    static void printHelp() {
        System.out.print(
                "Text chat server.\n\n" +
                        "USAGE:\n    <executable> [KEYS]\n\nKEYS:" +
                        "\n\n    -p, --port=PORT       Port to run on. 11111 by default." +
                        "\n\n    -j, --json            Server will use JSON data serialization. " +
                        "Server uses Java Object Serialization by default." +
                        "\n\n    -h, --help            Display help.\n\n"
        );
    }
}
