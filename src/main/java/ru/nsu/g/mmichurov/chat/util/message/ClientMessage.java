package ru.nsu.g.mmichurov.chat.util.message;

import java.io.Serializable;

public interface ClientMessage extends Serializable {

    final class Login implements ClientMessage {
        public final String name;
        public final String type;

        public Login(
                String name,
                String type) {
            this.name = name;
            this.type = type;
        }
    }

    final class Logout implements ClientMessage {
        public final int session;

        public Logout(int session) {
            this.session = session;
        }
    }

    final class List implements ClientMessage {
        public final int session;

        public List(int session) {
            this.session = session;
        }
    }

    final class Message implements ClientMessage {
        public final String message;
        public final int session;

        public Message(
                String message,
                int session) {
            this.message = message;
            this.session = session;
        }
    }
}
