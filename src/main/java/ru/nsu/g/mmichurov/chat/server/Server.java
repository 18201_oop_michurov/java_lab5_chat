package ru.nsu.g.mmichurov.chat.server;

import ru.nsu.g.mmichurov.chat.util.connection.Connection;
import ru.nsu.g.mmichurov.chat.util.connection.ConnectionListener;
import ru.nsu.g.mmichurov.chat.util.connection.exception.ProtocolViolationException;
import ru.nsu.g.mmichurov.chat.util.message.ClientMessage;
import ru.nsu.g.mmichurov.chat.util.message.ServerMessage;
import ru.nsu.g.mmichurov.chat.util.message.ServerMessage.Answer.Success.UsersList.User;
import ru.nsu.g.mmichurov.chat.util.serializer.ObjectSerializer;
import ru.nsu.g.mmichurov.chat.util.serializer.Serializer;
import ru.nsu.g.mmichurov.chat.util.serializer.exception.DeserializationException;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server implements Runnable {
    private static Serializer serializer = new ObjectSerializer();
    private static final Logger logger = Logger.getLogger(Server.class.getName());
    private final ServerSocket serverSocket;

    private final RequestHandler requestHandler = new RequestHandler();

    private final Queue<ServerMessage.Event.Message> messagesHistory;
    private static final int MAX_MESSAGES_HISTORY_SIZE = 20;

    private final HashSet<Client> authorizedClients;
    private final HashSet<String> occupiedNames;
    private final HashSet<Client> clientsToDrop;
    private int session = 0;

    public static void useSerializer(Serializer newSerializer) {
        serializer = newSerializer;
    }

    public Server(int port) throws IOException {
        this.serverSocket = new ServerSocket(port);

        this.messagesHistory = new ArrayDeque<>();

        this.authorizedClients = new HashSet<>();
        this.clientsToDrop = new HashSet<>();
        this.occupiedNames = new HashSet<>();

        logger.log(Level.INFO, "Server will be running on port {0}", port);
    }

    private int getUniqueSessionID() {
        this.session += 1;
        return this.session;
    }

    private void authoriseClient(Client client, String name, String type) {
        client.name = name;
        client.type = type;
        client.session = this.getUniqueSessionID();
        client.fullName = String.format("%s (%s)", client.name, client.fullAddress);
        client.loggedIn = true;

        this.authorizedClients.add(client);
        this.occupiedNames.add(client.name);

        logger.log(Level.INFO, "{0} logged in as {1}", new Object[]{client.fullAddress, client.name});

        this.sendAllExceptOne(new ServerMessage.Event.UserLogin(client.name), client);
    }

    private void dropClient(Client client) {
        logger.log(Level.INFO, "Dropped {0}", client.fullName);
        this.authorizedClients.remove(client);
        this.occupiedNames.remove(client.name);
    }

    private User[] getUsersList() {
        var list = new User[this.authorizedClients.size()];
        int i = 0;

        for (var client : this.authorizedClients) {
            list[i] = new User(client.name, client.type);
            i += 1;
        }

        return list;
    }

    private void send(Client client, ServerMessage message) throws IOException {
        client.connection.send(serializer.serialize(message, ServerMessage.class));
    }

    private void sendAll(ServerMessage message) {
        for (var client : this.authorizedClients) {
            try {
                this.send(client, message);
            } catch (IOException e) {
                this.clientsToDrop.add(client);
            }
        }

        this.authorizedClients.removeAll(this.clientsToDrop);
    }

    private void sendAllExceptOne(
            ServerMessage message,
            Client exclude) {
        for (var client : this.authorizedClients) {
            if (client != exclude) {
                try {
                    this.send(client, message);
                } catch (IOException e) {
                    this.clientsToDrop.add(client);
                }
            }
        }

        this.authorizedClients.removeAll(this.clientsToDrop);
    }

    private class RequestHandler {

        void handleRequest(Client client, ClientMessage clientMessage) throws IOException {
            if (client.loggedIn) {
                this.handleAuthorised(client, clientMessage);
            } else {
                this.handleUnauthorised(client, clientMessage);
            }
        }

        void handleAuthorised(Client client, ClientMessage clientMessage) throws IOException {
            if (clientMessage instanceof ClientMessage.Login) {
                Server.this.send(client, new ServerMessage.Answer.Error("You are already logged in as " + client.name));
            } else if (clientMessage instanceof ClientMessage.Logout) {
                this.handleLogoutRequest(client, (ClientMessage.Logout) clientMessage);
            } else if (clientMessage instanceof ClientMessage.List) {
                this.handleListRequest(client, (ClientMessage.List) clientMessage);
            } else if (clientMessage instanceof ClientMessage.Message) {
                this.handleMessage(client, (ClientMessage.Message) clientMessage);
            } else {
                Server.this.send(client, new ServerMessage.Answer.Error("Unknown request type"));
            }
        }

        void handleUnauthorised(Client client, ClientMessage clientMessage) throws IOException {
            if (clientMessage instanceof ClientMessage.Login) {
                this.handleLoginRequest(client, (ClientMessage.Login) clientMessage);
            } else {
                logger.log(Level.INFO, "Unauthorised request from {0}", client.fullName);
                Server.this.send(client, new ServerMessage.Answer.Error("You are not logged in"));
            }
        }

        void handleLoginRequest(Client client, ClientMessage.Login loginRequest) throws IOException {
            logger.log(Level.INFO, "Login request from {0} as {1}", new Object[]{client.fullName, loginRequest.name});
            if (client.loggedIn) {
                logger.log(Level.INFO, "Login request from {0}: already logged in", client.fullName);
                Server.this.send(client, new ServerMessage.Answer.Error("You are already logged in"));
                return;
            }

            if (Server.this.occupiedNames.contains(loginRequest.name)) {
                logger.log(Level.INFO, "Login request from {0}: name is already taken", client.fullName);
                Server.this.send(client, new ServerMessage.Answer.Error(
                        "Name \"" + loginRequest.name + "\" is already taken. Please choose another name"
                ));
                return;
            }
            if (loginRequest.name.isBlank()) {
                logger.log(Level.INFO, "Login request from {0}: invalid name", client.fullName);
                Server.this.send(client, new ServerMessage.Answer.Error("Invalid name"));
                return;
            }

            Server.this.authoriseClient(client, loginRequest.name, loginRequest.type);
            Server.this.send(client, new ServerMessage.Answer.Success.Session(client.session));
            for (var previousMessage : Server.this.messagesHistory) {
                Server.this.send(client, previousMessage);
            }
            Server.this.send(
                    client,
                    new ServerMessage.Event.Message("Server", "Welcome, " + client.name + "!")
            );
        }

        void handleLogoutRequest(Client client, ClientMessage.Logout logoutRequest) throws IOException {
            logger.log(Level.INFO, "Logout request from {0}", client.fullName);
            if (logoutRequest.session != client.session) {
                logger.log(Level.INFO, "Logout request from {0}: invalid session", client.fullName);
                Server.this.send(client, new ServerMessage.Answer.Error("Invalid session"));
                return;
            }

            Server.this.send(client, new ServerMessage.Answer.Success());
            try {
                client.connection.close();
            } catch (IOException ignored) {
            }
            /*Server.this.dropClient(client);*/
        }

        void handleListRequest(Client client, ClientMessage.List listRequest) throws IOException {
            logger.log(Level.INFO, "List request from {0}", client.fullName);
            if (listRequest.session != client.session) {
                logger.log(Level.INFO, "List request from {0}: invalid session", client.fullName);
                Server.this.send(client, new ServerMessage.Answer.Error("Invalid session"));
                return;
            }

            var usersList = Server.this.getUsersList();
            Server.this.send(client, new ServerMessage.Answer.Success.UsersList(usersList));
        }

        void handleMessage(Client client, ClientMessage.Message message) throws IOException {
            logger.log(Level.INFO, "Message from {0}", client.fullName);
            if (message.session != client.session) {
                logger.log(Level.INFO, "Message from {0}: invalid session", client.fullName);
                Server.this.send(client, new ServerMessage.Answer.Error("Invalid session"));
                return;
            }

            Server.this.send(client, new ServerMessage.Answer.Success());
            var messageToAll = new ServerMessage.Event.Message(client.name, message.message);
            Server.this.messagesHistory.add(messageToAll);
            if (Server.this.messagesHistory.size() > MAX_MESSAGES_HISTORY_SIZE) {
                Server.this.messagesHistory.poll();
            }
            Server.this.sendAllExceptOne(messageToAll, client);
        }
    }

    @Override
    public void run() {
        logger.log(Level.INFO, "Server is running");
        while (true) {
            try {
                var socket = this.serverSocket.accept();

                new Thread(() -> {
                    var connection = new Connection(socket);
                    var client = new Client(connection);
                    logger.log(Level.INFO, "Accepted new connection {0}", client.fullAddress);
                    var listener = new ClientConnectionListener(client, connection);
                    listener.run();
                }).start();
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    private static final class Client {
        final Connection connection;
        final String fullAddress;
        String fullName;
        boolean loggedIn = false;
        String name;
        String type;
        int session;

        Client(Connection connection) {
            this.connection = connection;
            this.fullAddress = String.format("%s:%d", this.connection.getInetAddress(), this.connection.getPort());
            this.fullName = this.fullAddress;
        }
    }

    private final class ClientConnectionListener extends ConnectionListener {
        private final Client client;

        ClientConnectionListener(Client client, Connection connection) {
            super(connection);
            this.client = client;
        }

        @Override
        public void onDataReceived(byte[] data) throws ProtocolViolationException {
            try {
                var clientMessage = serializer.deserialize(data, ClientMessage.class);
                Server.this.requestHandler.handleRequest(this.client, (ClientMessage) clientMessage);
            } catch (DeserializationException e) {
                throw new ProtocolViolationException("Invalid client data format", e);
            } catch (IOException e) {
                try {
                    this.client.connection.close();
                } catch (IOException ignored) {
                }
                Server.this.dropClient(this.client);
                Server.this.sendAll(new ServerMessage.Event.UserLogout(this.client.name));
            }
        }

        @Override
        public void onConnectionClosed() {
            logger.log(Level.INFO, "{0} disconnected", client.fullName);
            Server.this.dropClient(this.client);
            if (this.client.loggedIn) {
                Server.this.sendAll(new ServerMessage.Event.UserLogout(client.name));
            }
        }

        @Override
        public void onIOException(IOException e) {
            logger.log(Level.WARNING, this.client.fullAddress, e);
        }

        @Override
        public void onProtocolViolationException(ProtocolViolationException e) {
            logger.log(Level.WARNING, this.client.fullAddress, e);
            try {
                Server.this.send(this.client, new ServerMessage.Answer.Error("Protocol violation"));
            } catch (IOException ignored) {
            }
            try {
                this.client.connection.close();
            } catch (IOException ignored) {
            }
            logger.log(Level.WARNING, String.format("Connection with %s closed.", this.client.fullAddress));
        }
    }
}
