package ru.nsu.g.mmichurov.chat.client.ui;

import ru.nsu.g.mmichurov.chat.util.message.ServerMessage;

public interface ClientUI {

    void display(ServerMessage.Event event);

    void display(ServerMessage.Answer answer);

    void display(String message);

    void error(String message);

    void fatal(String message);
}
