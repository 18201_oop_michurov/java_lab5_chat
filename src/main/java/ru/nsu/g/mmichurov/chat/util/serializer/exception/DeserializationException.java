package ru.nsu.g.mmichurov.chat.util.serializer.exception;

import java.io.IOException;

public class DeserializationException extends IOException {

    public DeserializationException(String message) {
        super(message);
    }

    public DeserializationException(Throwable cause) {
        super(cause);
    }
}
